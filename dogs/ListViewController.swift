//
//  ListViewController.swift
//  dogs
//
//  Created by Hackintosh on 11/21/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var listLabel: UILabel!
}

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [String] = []
    var didEnterParameter: ((_ parameter: String) -> Void)?
    
    var cellReuseIdentifier = "cell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ListTableViewCell
        
        cell.listLabel.text = dataSource[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didEnterParameter?(dataSource[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
}
