//
//  UIImageView+loading.swift
//  dogs
//
//  Created by Hackintosh on 10/31/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

extension UIImageView {
    static var imageCache = [String: UIImage]()
    
    var cachedImageUrl: String {
        get {
            return ""
        }
        set {
            loadImg(urlString: newValue)
        }
    }
    
    func loadImg (urlString: String?) {
        var image: UIImage?
        
        guard let urlString = urlString else {
            return
        }
        if let cachedImage = UIImageView.imageCache[urlString] {
            image = cachedImage
            return self.image = image
        }
        if let url = URL(string: urlString) {
            DispatchQueue.global(qos: .userInitiated).async {
                guard let data = try? Data(contentsOf: url) else {
                    return
                }
                
                image = UIImage(data: data)
                
                UIImageView.imageCache[urlString] = image
                DispatchQueue.main.async {
                    self.image = image
                }
            }
        }
    }
}
