//
//  Data.swift
//  dogs
//
//  Created by Hackintosh on 10/25/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import Foundation
import UIKit

enum DataType {
    case StringValue
    case IntegerValue
    case FloatValue
    case SingleSelectValue
    case MultiSelectValue
    case ImageSingleSelectValue
    case SwitchValue
}

struct DataRow {
    var dataType: DataType
    var fieldName: String
    var value: AnyObject?
    var petType: PetType?
    var listToSelect:[String]?
}

var dogForm: Array <DataRow> = [
    DataRow(dataType: .StringValue, fieldName: "Name", value: nil, petType: .dog, listToSelect: nil),
    DataRow(dataType: .ImageSingleSelectValue, fieldName: "Breed", value: nil, petType: .dog, listToSelect: nil),
    DataRow(dataType: .IntegerValue, fieldName: "Age", value: nil, petType: .dog, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Gender", value: nil, petType: .dog, listToSelect: list["Gender"]),
    DataRow(dataType: .IntegerValue, fieldName: "Number of curls", value: nil, petType: .dog, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Kind of ears", value: nil, petType: .dog, listToSelect: list["EarsKind"]),
    DataRow(dataType: .SwitchValue, fieldName: "Is sterilized?", value: false as AnyObject, petType: .dog, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Kind of feed", value: nil, petType: .dog, listToSelect: list["FeedKind"]),
]

var catForm: Array <DataRow> = [
    DataRow(dataType: .StringValue, fieldName: "Name", value: nil, petType: .cat, listToSelect: nil),
    DataRow(dataType: .ImageSingleSelectValue, fieldName: "Breed", value: nil, petType: .cat, listToSelect: nil),
    DataRow(dataType: .IntegerValue, fieldName: "Age", value: nil, petType: .dog, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Gender", value: nil, petType: .cat, listToSelect: list["Gender"]),
    DataRow(dataType: .SwitchValue, fieldName: "Is sterilized?", value: false as AnyObject, petType: .cat, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Kind of feed", value: nil, petType: .cat, listToSelect: list["FeedKind"]),
]

var birdForm: Array <DataRow> = [
    DataRow(dataType: .StringValue, fieldName: "Name", value: nil, petType: .bird, listToSelect: nil),
    DataRow(dataType: .ImageSingleSelectValue, fieldName: "Breed", value: nil, petType: .bird, listToSelect: nil),
    DataRow(dataType: .IntegerValue, fieldName: "Age", value: nil, petType: .bird, listToSelect: nil),
    DataRow(dataType: .SingleSelectValue, fieldName: "Gender", value: nil, petType: .bird, listToSelect: list["Gender"]),
    DataRow(dataType: .FloatValue, fieldName: "Wingspan", value: nil, petType: .bird, listToSelect: nil),
    DataRow(dataType: .FloatValue, fieldName: "Beak length", value: nil, petType: .bird, listToSelect: nil),
    DataRow(dataType: .SwitchValue, fieldName: "Ability to sing", value: false as AnyObject, petType: .bird, listToSelect: nil),
    DataRow(dataType: .SwitchValue, fieldName: "Ability to speak", value: false as AnyObject, petType: .bird, listToSelect: nil),
]

enum PetType: String {
    case dog = "Dog"
    case cat = "Cat"
    case bird = "Bird"
    
    static let allValues = [dog, cat, bird]
}

let list = ["EarsKind": ["Hanging", "Erect"], "FeedKind": ["Natural", "Dry food", "Wet food"], "Gender": ["Male", "Female"]]

let breeds: Array <BreedParameters> = [
    BreedParameters(breedType: .dog, breedName: "Pug", imgUrl: "https://img1.goodfon.ru/wallpaper/big/7/ef/mops-morda-glaza.jpg"),
    BreedParameters(breedType: .dog, breedName: "Bulldog", imgUrl: "https://dogsecrets.ru/wp-content/uploads/2015/07/191-Angliyskiy-buldog-foto.jpg"),
    BreedParameters(breedType: .dog, breedName: "Bigl", imgUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMTZlIOGTvSUgUysMH9JuwvCfQh0WDN1b8zGxcgi3sZS-YcashJQ"),
    BreedParameters(breedType: .dog, breedName: "Chihuahua", imgUrl: "https://dogsecrets.ru/images/stories/123/chihuahua/chi2.jpg"),
    BreedParameters(breedType: .dog, breedName: "Pooch", imgUrl: nil),
    
    BreedParameters(breedType: .cat, breedName: "British cat", imgUrl: "https://aroundpet.ru/wp-content/uploads/britanskaya-koshka-istoriya-proishozhdeniya-1170x750.jpg"),
    BreedParameters(breedType: .cat, breedName: "Exot", imgUrl: "https://pets2.me/media/res/4/2/6/4/4264.ox1ifo.300.jpg"),
    BreedParameters(breedType: .cat, breedName: "Siamese cat", imgUrl: "https://upload.wikimedia.org/wikipedia/commons/1/16/Siamese_cat_Vaillante.JPG"),
    BreedParameters(breedType: .cat, breedName: "Pooch", imgUrl: nil),
    
    BreedParameters(breedType: .bird, breedName: "Budgerigal", imgUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Budgerigar_1_%2820123571788%29.jpg/275px-Budgerigar_1_%2820123571788%29.jpg"),
    BreedParameters(breedType: .bird, breedName: "Owl", imgUrl: "https://zveri.guru/images/252549/otlichie.jpg"),
    BreedParameters(breedType: .bird, breedName: "Raven", imgUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Common_Raven_Grand_Canyon_1.jpg/275px-Common_Raven_Grand_Canyon_1.jpg"),
    BreedParameters(breedType: .bird, breedName: "Ara parrot", imgUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRg1tu1jS55wADJECpfap2gIKGDmFDdudLiAPNjT5TjjuWAdH-8"),
]

struct BreedParameters {
    var breedType: PetType
    var breedName: String
    var imgUrl: String?
}

var pets: Array<Array <DataRow>> = []





