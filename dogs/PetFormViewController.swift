//
//  ViewController.swift
//  dogs
//
//  Created by Hackintosh on 10/9/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit


class NamePetFormTableViewCell: UITableViewCell {
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
}

class BreedPetFormTableViewCell: UITableViewCell {
    @IBOutlet weak var imgBreed: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

class SwitchPetFormTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchView: UIView!
    weak var switcher: SwitcherView!
    
    override func awakeFromNib() {
        initSubviews()
    }
    
    func initSubviews() {
        let switcher = SwitcherView(frame: CGRect(x: 0, y: 0, width: 75, height: 44))
        switchView.addSubview(switcher)
        self.switcher = switcher
    }
}

class PetFormViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var petType: PetType?
    var index: Int?
    var switcherIndex:Int?
    
    lazy var petForm = getPetForm()
    
    func getPetForm () -> [DataRow] {
        switch petType {
        case .dog?:
            return dogForm
        case .cat?:
            return catForm
        case .bird?:
            return birdForm
        default:
            return []
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func buttonSavePressed(_ sender: Any) {
        for row in petForm {
            switch row.fieldName {
            case "Name":
                guard row.value != nil else {
                    sendAlert(message: "Enter your pet's name")
                    return
                }
                break
            case "Breed":
                guard row.value != nil else {
                    sendAlert(message: "Choose your pet's breed")
                    return
                }
                break
            default:
                break
            }
        }
        if let index = index {
            pets[index] = petForm
        }
        else {
            pets.append(petForm)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receiveData"), object: petType)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func sendAlert (message: String) {
        let alert = UIAlertController(title: "Ooops!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petForm.count
    }
    
    func namesValuesDisplay (index: Int) -> NamePetFormTableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "nameTableViewCell") as! NamePetFormTableViewCell
        cell.titleLabel.text = petForm[index].fieldName
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch petForm[indexPath.row].dataType {
        case .StringValue, .SingleSelectValue:
            let cell = namesValuesDisplay(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? String {
                cell.valueLabel.text = String(value)
            }
            return cell
        case .FloatValue:
            let cell = namesValuesDisplay(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? Float {
                cell.valueLabel.text = String(value)
            }
            return cell
        case .IntegerValue:
            let cell = namesValuesDisplay(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? Int {
                cell.valueLabel.text = String(value)
            }
            return cell
        case .ImageSingleSelectValue:
            let cell:BreedPetFormTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "breedTableViewCell") as! BreedPetFormTableViewCell
            cell.titleLabel.text = petForm[indexPath.row].fieldName
            guard let breedData = petForm[indexPath.row].value as? BreedParameters else { return cell }
            cell.imgBreed.cachedImageUrl = breedData.imgUrl ?? ""
            return cell
        case .SwitchValue:
            let cell:SwitchPetFormTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "switchTableViewCell") as! SwitchPetFormTableViewCell
            cell.titleLabel.text = petForm[indexPath.row].fieldName
            if let value = petForm[indexPath.row].value as? Bool {
                cell.switcher.isOn = value
                cell.switcher.addTarget(self, action: #selector(stateChanged), for: UIControlEvents.valueChanged)
                cell.switcher.tag = indexPath.row
            }
            return cell
        default:
            fatalError("Unexpected section \(indexPath.section)")
        }
    }
    
    @objc func stateChanged(switcher: SwitcherView) {
        petForm[switcher.tag].value = switcher.isOn as AnyObject
    }
    
    func setTitle(vc: UIViewController) {
        for row in petForm {
            switch row.fieldName {
            case "Name":
                if let value = row.value as? String {
                    vc.title = String(value)
                }
                else {
                    vc.title = ""
                }
                break
            default:
                break
            }
        }
    }
    
    func namesValuesSelected (index: Int) -> NamesViewController {
        let vc = storyboard?.instantiateViewController(withIdentifier: "IDNamesViewController") as! NamesViewController
        setTitle(vc: vc)
        vc.didEnterParameter = { [weak self] parameter in
            self?.petForm[index].value = parameter
            self?.tableView.reloadData()
        }
        navigationController?.pushViewController(vc, animated: true)
        return vc
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch petForm[indexPath.row].dataType {
        case .StringValue:
            let vc = namesValuesSelected(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? String {
                vc.inputType = .string(value)
            }
            else {
                vc.inputType = .string("")
            }
        case .FloatValue:
            let vc = namesValuesSelected(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? Float {
                vc.inputType = .float(value)
            }
            else {
                vc.inputType = .float(nil)
            }
        case .IntegerValue:
            let vc = namesValuesSelected(index: indexPath.row)
            if let value = petForm[indexPath.row].value as? Int {
                vc.inputType = .int(value)
            }
            else {
                vc.inputType = .int(nil)
            }
        case .ImageSingleSelectValue:
            let vc = storyboard?.instantiateViewController(withIdentifier: "IDBreedsViewController") as! BreedsViewController
            setTitle(vc: vc)
            vc.didEnterBreed = { [weak self] breedData in
                self?.petForm[indexPath.row].value = breedData as AnyObject
                tableView.reloadData()
            }
            vc.dataSource = breeds.filter { $0.breedType == petForm[indexPath.row].petType}
            
            navigationController?.pushViewController(vc, animated: true)
        case .SingleSelectValue:
            let vc = storyboard?.instantiateViewController(withIdentifier: "IDListViewController") as! ListViewController
            setTitle(vc: vc)
            vc.didEnterParameter = { [weak self] parameter in
                self?.petForm[indexPath.row].value = parameter as AnyObject
                self?.tableView.reloadData()
            }
            if let list = petForm[indexPath.row].listToSelect {
                vc.dataSource = list
            }
            navigationController?.pushViewController(vc, animated: true)
        default:
            fatalError("Unexpected section \(indexPath.section)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
