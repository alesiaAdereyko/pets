//
//  NamesViewController.swift
//  dogs
//
//  Created by Hackintosh on 10/9/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

enum inputType {
    case string (String)
    case float (Float?)
    case int (Int?)
}

class NamesViewController: UIViewController {
    
    var inputType: inputType?
    var didEnterParameter: ((_ parameter: AnyObject) -> Void)?
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func buttonSavePressed(_ sender: Any) {
        switch inputType {
        case .float?:
            guard Float(textField.text!) != nil else {
                return sendAlert(message: "This parameter should be float")
            }
        case .int?:
            guard Int(textField.text!) != nil else {
                return sendAlert(message: "This parameter should be integer")
            }
        default:
            break
        }
        var parameter: AnyObject? {
            switch self.inputType {
            case .string?:
                if let parameter = textField.text {
                    return parameter as AnyObject
                }
                else {
                    return nil
                }
            case .float?:
                if let parameter = Float(textField.text!) {
                    return parameter as AnyObject
                }
                else {
                    return nil
                }
            case .int?:
                if let parameter = Int(textField.text!) {
                    return parameter as AnyObject
                }
                else {
                    return nil
                }
            default:
                return nil
            }
        }
        didEnterParameter?(parameter as AnyObject)
        navigationController?.popViewController(animated: true)
    }
    
    func sendAlert (message: String) {
        let alert = UIAlertController(title: "Ooops!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch inputType {
        case .string(let str)?:
            textField.text = str
        case .float(let flt)?:
            if let flt = flt {
                textField.text = String(flt)
            }
            else {
                textField.text = ""
            }
        case .int(let int)?:
            if let int = int {
                textField.text = String(int)
            }
            else {
                textField.text = ""
            }
        default:
            textField.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
