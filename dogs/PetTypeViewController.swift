//
//  PetTypeViewController.swift
//  dogs
//
//  Created by Hackintosh on 10/16/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

class PetTypeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Pet's type"
    }
    
    func buttonPressed (type: PetType) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PetFormViewController") as! PetFormViewController
        vc.petType = type
        vc.title = vc.petType?.rawValue
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func dogButtonPressed(_ sender: Any) {
        buttonPressed(type: .dog)
    }
    @IBAction func catButtonPressed(_ sender: Any) {
        buttonPressed(type: .cat)
    }
    @IBAction func birdButtonPressed(_ sender: Any) {
        buttonPressed(type: .bird)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
