//
//  RootController.swift
//  dogs
//
//  Created by Hackintosh on 10/16/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

class RootTableViewCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var breed: UILabel!
}

class RootController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var cellReuseIdentifier = "petCell"
    var petType: PetType?
    var index: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Pets"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(plusButtonPressed))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(notification:)), name: NSNotification.Name(rawValue: "receiveData"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func onDidReceiveData(notification:Notification) {
        petType = notification.object as? PetType
        tableView.reloadData()
    }
    
    @objc func plusButtonPressed(sender: UIButton) {
        let petTypeVC = storyboard?.instantiateViewController(withIdentifier: "IDPetTypeViewController") as! PetTypeViewController
        navigationController?.pushViewController(petTypeVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:RootTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RootTableViewCell
        
        for pet in pets[indexPath.row] {
            switch pet.fieldName {
            case "Name":
                if let value = pet.value as? String {
                    cell.name.text = String(value)
                }
                else {
                    cell.name.text = ""
                }
            case "Breed":
                if let value = pet.value as? BreedParameters {
                    cell.breed.text = value.breedName
                    cell.img.cachedImageUrl = value.imgUrl ?? ""
                }
            default:
                break
            }
        }
        cell.type.text = pets[indexPath.row][0].petType?.rawValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let petFormVC = storyboard?.instantiateViewController(withIdentifier: "PetFormViewController") as! PetFormViewController
        petFormVC.index = indexPath.row
        petFormVC.petForm = pets[indexPath.row]
        navigationController?.pushViewController(petFormVC, animated: true)
    }
    
}

