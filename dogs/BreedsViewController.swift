//
//  BreedsViewController.swift
//  dogs
//
//  Created by Hackintosh on 10/9/18.
//  Copyright © 2018 Hackintosh. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
}

class BreedsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var cellReuseIdentifier = "cell"
    var didEnterBreed: ((_ breedData: BreedParameters) -> Void)?
    
    var dataSource: [BreedParameters] = []


    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TableViewCell
        
        cell.name.text = dataSource[indexPath.row].breedName
        cell.img.cachedImageUrl = dataSource[indexPath.row].imgUrl ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didEnterBreed?(dataSource[indexPath.row])
        navigationController?.popViewController(animated: true)
    }

}
